# Production Installation 

## Manual method

This section describes how to set up WA-Backstage in a step-by-step guide. If you are familiar with
Ansible and do not care about details, you may fast-forward to [Ansible method](#ansible-method).
### Requirements

* Docker, Docker-Compose
* NGINX for TLS termination
* a copy of the [deployment folder](./deployment)


### 1. Create folder
Create a new folder on your webserver and name it backstage:

```
cd /opt
mkdir backstage
```


### 2. Copy over the contents of the deployment folder

* [docker-compose.yaml](./deployment/docker-compose.yaml)
* [local-settings.py](./deployment/local-settings.py)
* [nginx.conf](./deployment/nginx.conf)
* [create_admin_tag.py](./deployment/create_admin_tag.py)


### 3. Change the templates

In `docker-compose.yaml` set the following environment variables of the `db` container:

* `POSTGRES_DB`: Desired name of your the PostgreSQL database
* `POSTGRES_USER`: Name of the PostgreSQL user that owns the database
* `POSTGRES_PASSWORD`: Password for the PostgreSQL user (use a random string of >= 20 characters)

In `local-settings.py` set these variables:

* `NAME`, `USER` and `PASSWORD`: The same values you chose for `docker-compose.yaml`
* `ALLOWED_HOSTS`: The fully-qualified domain name of your WA-Backstage instance
                   (This must be an array, so make sure to keep the square brackets!)
* `SECRET_KEY`: A random string >= 32 characters
* `ADMIN_API_TOKEN`: A random string >= 32 characters
* `SITE_URL`: The root url of the domain you are running backstage on
* (optionally) `EMAIL_*` settings to enable WA-Backstage to send emails

The Django secret key is used to for cryptographic operations like signing session cookies.

The Admin-API-Token is the password that connects WorkAdventure instances with WorkAdventure
Backstage. Make sure the same Admin-API-Token is also set in the `.env`-file in your WorkAdventure
installation, along with the FQDN you entered in `ALLOWED_HOSTS`:

```
ADMIN_API_URL=<ALLOWED_HOSTS>
ADMIN_API_TOKEN=<ADMIN_API_TOKEN>
```


### 4. Start the containers

Make sure you are in your `backstage` folder and fire up your WA-Backstage instancee:

```
docker-compose up -d
```

Verify everybody is happy:

```
docker-compose ps
```

This should now list three new Docker containers called

* `backstage_app`,
* `backstage_db` and
* `backstage_nginx`.


### 5. Create an admin user

In order to login to the your WA-Backstage instance, you need to create an admin user. To do this,
make sure you are still in your `backstage` folder and run:

```
docker-compose exec app ./manage.py createsuperuser
```

The command will ask you for user credentials.


### 6. Create the admin tag

The admin tag is necessary to assign special admin rights to a user (e.g. calls in WorkAdventure).

Make sure you are in your `backstage` folder and enter:

```
docker-compose exec app ./manage.py create_admin_tag
```


### 7. Login

Log in under the domain you have provided in `local-settings.py` with the credentials you set in
step 5. Rejoice! \\o/



## Ansible method

### Requirements

* Ansible
* Docker-compose installed on the target host
* [WA-Backstage Ansible Role](https://git.fnordkollektiv.de/pub/ansible/workadventure_backstage)

### Installation
Please refer to README.md in the Ansible role repository.
