# python import
import uuid
from urllib import parse as urlparse

# django imports
from rest_framework import serializers
from rest_framework.relations import RelatedField
from rest_framework.exceptions import APIException, ValidationError
from django.core.exceptions import ObjectDoesNotExist

# project imports
from core.models import Ban, CharacterTexture, MembershipWorld, Report, Room, World, Player


class BanSerializer(serializers.ModelSerializer):
    is_banned = serializers.SerializerMethodField()
    message = serializers.CharField()

    def __init__(self, instance=None, is_banned=False, **kwargs):
        self.is_banned = is_banned
        super().__init__(instance=instance, **kwargs)

    def get_is_banned(self, obj):
        return self.is_banned

    class Meta:
        model = Ban
        fields = ["is_banned", "message"]


class TextureSerializer(serializers.ModelSerializer):
    vars()["id"] = serializers.IntegerField(source="texture_id")
    level = serializers.IntegerField()
    # rights = serializers.StringRelatedField(many=True)
    rights = serializers.SerializerMethodField(method_name="get_rights")
    url = serializers.URLField()

    def get_rights(self, obj):
        """
        This logic is random. We need to end up with a string.
        Attribute not used in WA
        """
        return " "

    class Meta:
        model = CharacterTexture
        fields = ["id", "level", "rights", "url"]


class MapSerializer(serializers.ModelSerializer):
    mapUrl = serializers.URLField(source="map_url")
    policy_type = serializers.IntegerField()
    tags = serializers.StringRelatedField(many=True)
    textures = TextureSerializer(many=True, source="world.character_textures")
    # deprecated, but needs to exist atm 02/22
    roomSlug = serializers.SerializerMethodField(method_name="get_room_slug")
    authenticationMandatory = serializers.SerializerMethodField(method_name="get_authentication_mandatory")
    contactPage = serializers.ReadOnlyField(source="contact_page")
    # used for some analytics functionality, came across 09/21
    group = serializers.ReadOnlyField(source="analytics_group")

    def get_room_slug(self, obj):
        return ""

    def get_authentication_mandatory(self, obj):
        """
        If the user is anonymous, we turn of oauth even if configured on the world
        """
        if obj.policy_type == 1:
            return False
        else:
            return obj.world.use_oauth

    class Meta:
        model = Room
        fields = [
            "mapUrl",
            "policy_type",
            "tags",
            "textures",
            "roomSlug",
            "authenticationMandatory",
            "contactPage",
            "group",
        ]


class ReportUserRelatedField(RelatedField):
    def to_representation(self, value):
        if self.parent.instance:
            # we currently don't need to get report representations
            # and also it is difficult to get the related world in this scope
            return ""
        else:
            # do not break drf html pages
            return ""

    def to_internal_value(self, data):
        # validate
        try:
            data_uuid = uuid.UUID(data)
        except ValueError:
            raise ValidationError("Value not a uuid")
        # query
        player = Player.objects.get(world_memberships__uuid_token=data_uuid)
        if player:
            return player
        else:
            raise ValidationError("Player does not exist")


class RelativeUrlRelatedField(serializers.SlugRelatedField):
    def to_internal_value(self, data):
        # we have to cut the domain part if it comes along
        data = urlparse.urlparse(data).path
        return super().to_internal_value(data)


class ReportSerializer(serializers.ModelSerializer):
    reportWorldSlug = RelativeUrlRelatedField(source="room", slug_field="url", queryset=Room.objects.all())
    reportedUserComment = serializers.CharField(source="user_comment")
    reportedUserUuid = ReportUserRelatedField(source="reported_user", queryset=Player.objects.all())
    reporterUserUuid = ReportUserRelatedField(source="reporter_user", queryset=Player.objects.all())

    class Meta:
        model = Report
        fields = ["reportWorldSlug", "reportedUserComment", "reportedUserUuid", "reporterUserUuid"]


class RoomAccessSerializer(serializers.ModelSerializer):
    email = serializers.SlugRelatedField(source="player", slug_field="email", queryset=Player.objects.all())
    userUUID = serializers.UUIDField(source="uuid_token")
    tags = serializers.StringRelatedField(many=True)
    visitCardUrl = serializers.CharField(source="visit_card_url")
    textures = TextureSerializer(many=True, source="character_textures")
    # messages = SerializerMethodField(method_name="get_messages")
    anonymous = serializers.BooleanField()

    # TBD implement welcome messages -> epic
    # def get_messages(self, obj):
    #     import json

    #     messages = json.dumps(
    #         {"ops": [{"insert": "wtf\n"}, {"insert": "even with colors", "attributes": {"color": "#f00"}}]}
    #     )

    #     return [
    #         {"type": "message", "message": messages},
    #     ]

    class Meta:
        model = MembershipWorld
        fields = ["email", "userUUID", "tags", "textures", "anonymous", "visitCardUrl"]


class AdminApiDataSerializer(serializers.ModelSerializer):
    email = serializers.SlugRelatedField(source="player", slug_field="email", queryset=Player.objects.all())
    userUuid = serializers.UUIDField(source="uuid_token")
    tags = serializers.StringRelatedField(many=True)
    textures = TextureSerializer(many=True, source="character_textures")
    # wtf is messages?
    roomUrl = serializers.SerializerMethodField()
    # is this really used somewhere?
    mapUrlStart = serializers.SlugRelatedField(source="world", slug_field="url", queryset=World.objects.all())

    def get_roomUrl(self, obj):
        try:
            room = Room.objects.get(world=obj.world, is_entry_room=True)
            return room.url
        except ObjectDoesNotExist:
            raise APIException("World has no entry room configured.")

    def get_policy_type(self, obj):
        try:
            room = Room.objects.get(world=obj.world, is_entry_room=True)
            return room.policy_type
        except ObjectDoesNotExist:
            raise APIException("World has no entry room configured.")

    class Meta:
        model = MembershipWorld
        fields = ["roomUrl", "email", "mapUrlStart", "tags", "userUuid", "textures"]


class RoomSameWorldListSerializer(serializers.ListSerializer):
    child = serializers.CharField()

    def to_representation(self, data):
        return [room.world.url + room.url for room in data]

    class Meta:
        models = Room


class RoomSameWorldSerializer(MapSerializer):
    class Meta:
        model = Room
        fields = ["mapUrl", "policy_type", "tags", "textures"]
        list_serializer_class = RoomSameWorldListSerializer
