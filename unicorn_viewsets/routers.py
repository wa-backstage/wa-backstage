from django.urls import path


class Router:
    def __init__(self, view_class, root_url_name=None):
        self.root_url_name = root_url_name
        self.view_class = view_class
        self.registry = []

    def get_routes(self):
        return [
            # list
            ("", ""),
            # create
            ("add/", "-create"),
            # update
            ("<str:pk>/edit/", "-update"),
        ]

    def register(self, prefix, component_name):
        self.registry.append((prefix, component_name))

    @property
    def urls(self):
        urls = []
        if self.root_url_name:
            urls.append(path("", self.view_class.as_view(), name=self.root_url_name))
        for prefix, component_name in self.registry:
            for route_suffix, name_suffix in self.get_routes():
                route = f"{prefix}/{route_suffix}"
                name = f"{component_name}{name_suffix}"
                view = self.view_class.as_view(component_name=component_name)
                urls.append(path(route, view, name=name))
        return urls
