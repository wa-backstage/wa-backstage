class CreateModeMixin:
    def show_create(self):
        return self.switch_mode("create")

    def validate_and_create(self):
        return self.validate_and_save_form()


class DeleteModeMixin:
    def delete(self, pk):
        self.get_queryset().get(pk=pk).delete()
        return self.switch_mode()


class UpdateModeMixin:
    def show_update(self, pk):
        return self.switch_mode("update", pk=pk)

    def validate_and_update(self):
        return self.validate_and_save_form()
