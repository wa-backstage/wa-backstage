# Components

> Auto-generated documentation for [frontend.components](blob/master/frontend/components/__init__.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / Components
    - Modules
        - [Backstage Users](backstage_users.md#backstage-users)
        - [Bans](bans.md#bans)
        - [Character Textures](character_textures.md#character-textures)
        - [Players](players.md#players)
        - [Profile](profile.md#profile)
        - [Rooms](rooms.md#rooms)
        - [Tags](tags.md#tags)
        - [World Memberships](world_memberships.md#world-memberships)
        - [Worlds](worlds.md#worlds)
