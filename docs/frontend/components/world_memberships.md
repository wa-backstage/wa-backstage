# World Memberships

> Auto-generated documentation for [frontend.components.world_memberships](blob/master/frontend/components/world_memberships.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / [Components](index.md#components) / World Memberships
    - [WorldMembershipsView](#worldmembershipsview)
        - [WorldMembershipsView().create_membership](#worldmembershipsviewcreate_membership)
        - [WorldMembershipsView().get_form_data](#worldmembershipsviewget_form_data)
        - [WorldMembershipsView().get_form_kwargs](#worldmembershipsviewget_form_kwargs)
        - [WorldMembershipsView().get_mode_kwargs](#worldmembershipsviewget_mode_kwargs)
        - [WorldMembershipsView().get_queryset](#worldmembershipsviewget_queryset)
        - [WorldMembershipsView().get_success_url](#worldmembershipsviewget_success_url)
        - [WorldMembershipsView().set_attributes](#worldmembershipsviewset_attributes)
        - [WorldMembershipsView().toggle_create](#worldmembershipsviewtoggle_create)
        - [WorldMembershipsView().validate_and_save_form](#worldmembershipsviewvalidate_and_save_form)

## WorldMembershipsView

[[find in source code]](blob/master/frontend/components/world_memberships.py#L11)

```python
FilterSelectWidgetDecorator(
    queryset=Tag.objects.exclude(name='admin'),
    form_field='tags',
)
class WorldMembershipsView(UpdateModeMixin, DeleteModeMixin, GenericModeView):
    def __init__(**kwargs):
```

#### See also

- [DeleteModeMixin](../../unicorn_viewsets/mixins.md#deletemodemixin)
- [GenericModeView](../../unicorn_viewsets/views.md#genericmodeview)
- [UpdateModeMixin](../../unicorn_viewsets/mixins.md#updatemodemixin)

### WorldMembershipsView().create_membership

[[find in source code]](blob/master/frontend/components/world_memberships.py#L30)

```python
def create_membership(player_pk):
```

### WorldMembershipsView().get_form_data

[[find in source code]](blob/master/frontend/components/world_memberships.py#L44)

```python
def get_form_data():
```

At this point this looks like an empty return. However, this method will be modified by the decorators.

### WorldMembershipsView().get_form_kwargs

[[find in source code]](blob/master/frontend/components/world_memberships.py#L48)

```python
def get_form_kwargs():
```

### WorldMembershipsView().get_mode_kwargs

[[find in source code]](blob/master/frontend/components/world_memberships.py#L35)

```python
def get_mode_kwargs():
```

### WorldMembershipsView().get_queryset

[[find in source code]](blob/master/frontend/components/world_memberships.py#L38)

```python
def get_queryset():
```

### WorldMembershipsView().get_success_url

[[find in source code]](blob/master/frontend/components/world_memberships.py#L41)

```python
def get_success_url():
```

### WorldMembershipsView().set_attributes

[[find in source code]](blob/master/frontend/components/world_memberships.py#L54)

```python
def set_attributes():
```

### WorldMembershipsView().toggle_create

[[find in source code]](blob/master/frontend/components/world_memberships.py#L26)

```python
def toggle_create():
```

### WorldMembershipsView().validate_and_save_form

[[find in source code]](blob/master/frontend/components/world_memberships.py#L62)

```python
def validate_and_save_form():
```
