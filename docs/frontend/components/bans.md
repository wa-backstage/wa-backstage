# Bans

> Auto-generated documentation for [frontend.components.bans](blob/master/frontend/components/bans.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / [Components](index.md#components) / Bans
    - [BansView](#bansview)
        - [BansView().get_form_data](#bansviewget_form_data)
        - [BansView().get_form_kwargs](#bansviewget_form_kwargs)
        - [BansView().get_mode_kwargs](#bansviewget_mode_kwargs)
        - [BansView().get_queryset](#bansviewget_queryset)
        - [BansView().get_success_url](#bansviewget_success_url)
        - [BansView().set_attributes](#bansviewset_attributes)

## BansView

[[find in source code]](blob/master/frontend/components/bans.py#L9)

```python
class BansView(
    ReferralMixin,
    CreateModeMixin,
    UpdateModeMixin,
    DeleteModeMixin,
    GenericModeView,
):
    def __init__(**kwargs):
```

#### See also

- [CreateModeMixin](../../unicorn_viewsets/mixins.md#createmodemixin)
- [DeleteModeMixin](../../unicorn_viewsets/mixins.md#deletemodemixin)
- [GenericModeView](../../unicorn_viewsets/views.md#genericmodeview)
- [ReferralMixin](../views.md#referralmixin)
- [UpdateModeMixin](../../unicorn_viewsets/mixins.md#updatemodemixin)

### BansView().get_form_data

[[find in source code]](blob/master/frontend/components/bans.py#L37)

```python
def get_form_data():
```

### BansView().get_form_kwargs

[[find in source code]](blob/master/frontend/components/bans.py#L42)

```python
def get_form_kwargs():
```

### BansView().get_mode_kwargs

[[find in source code]](blob/master/frontend/components/bans.py#L28)

```python
def get_mode_kwargs():
```

### BansView().get_queryset

[[find in source code]](blob/master/frontend/components/bans.py#L31)

```python
def get_queryset():
```

### BansView().get_success_url

[[find in source code]](blob/master/frontend/components/bans.py#L34)

```python
def get_success_url():
```

### BansView().set_attributes

[[find in source code]](blob/master/frontend/components/bans.py#L47)

```python
def set_attributes():
```
