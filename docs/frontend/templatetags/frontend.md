# Frontend

> Auto-generated documentation for [frontend.templatetags.frontend](blob/master/frontend/templatetags/frontend.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Frontend](../index.md#frontend) / [Templatetags](index.md#templatetags) / Frontend
    - [aselementid](#aselementid)
    - [concaturlwith](#concaturlwith)
    - [filterselect_widget](#filterselect_widget)
    - [ifnoroomall](#ifnoroomall)
    - [isopenforall](#isopenforall)
    - [joinnoadmin](#joinnoadmin)
    - [openforall](#openforall)
    - [prepend_url](#prepend_url)
    - [replaceextension](#replaceextension)
    - [startswith](#startswith)
    - [withparam](#withparam)
    - [withreferrer](#withreferrer)
    - [withstringparam](#withstringparam)

## aselementid

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L12)

```python
@register.filter
def aselementid(value, arg):
```

## concaturlwith

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L37)

```python
@register.filter
def concaturlwith(value, arg):
```

## filterselect_widget

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L72)

```python
@register.inclusion_tag(
    'frontend/partials/filterselect_widget.html',
    takes_context=True,
)
def filterselect_widget(context, form_field, label, name_plural):
```

## ifnoroomall

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L57)

```python
@register.filter
def ifnoroomall(value):
```

## isopenforall

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L62)

```python
@register.filter
def isopenforall(value):
```

## joinnoadmin

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L47)

```python
@register.filter
def joinnoadmin(value):
```

## openforall

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L42)

```python
@register.filter
def openforall(value):
```

## prepend_url

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L52)

```python
@register.filter
def prepend_url(value, arg):
```

## replaceextension

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L67)

```python
@register.filter
def replaceextension(value, arg):
```

## startswith

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L17)

```python
@register.filter
def startswith(value, arg):
```

## withparam

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L22)

```python
@register.filter
def withparam(value, arg):
```

## withreferrer

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L27)

```python
@register.filter
def withreferrer(value, arg):
```

## withstringparam

[[find in source code]](blob/master/frontend/templatetags/frontend.py#L32)

```python
@register.filter
def withstringparam(value, arg):
```
