# Admin

> Auto-generated documentation for [frontend.admin](blob/master/frontend/admin.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Frontend](index.md#frontend) / Admin
    - [PlayerResource](#playerresource)
        - [PlayerResource().after_save_instance](#playerresourceafter_save_instance)
        - [PlayerResource().dehydrate_access_url](#playerresourcedehydrate_access_url)
        - [PlayerResource().get_queryset](#playerresourceget_queryset)
    - [import_players](#import_players)

## PlayerResource

[[find in source code]](blob/master/frontend/admin.py#L19)

```python
class PlayerResource(resources.ModelResource):
    def __init__(world=None):
```

### PlayerResource().after_save_instance

[[find in source code]](blob/master/frontend/admin.py#L34)

```python
def after_save_instance(instance, using_transactions, dry_run):
```

### PlayerResource().dehydrate_access_url

[[find in source code]](blob/master/frontend/admin.py#L38)

```python
def dehydrate_access_url(player):
```

### PlayerResource().get_queryset

[[find in source code]](blob/master/frontend/admin.py#L41)

```python
def get_queryset():
```

## import_players

[[find in source code]](blob/master/frontend/admin.py#L10)

```python
def import_players(field_file: FieldFile, world=None, dry_run=False):
```
