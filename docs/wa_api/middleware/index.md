# Middleware

> Auto-generated documentation for [wa_api.middleware](blob/master/wa_api/middleware/__init__.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Wa Api](../index.md#wa-api) / Middleware
    - Modules
        - [Pre Shared Key Auth](pre_shared_key_auth.md#pre-shared-key-auth)
