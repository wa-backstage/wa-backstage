# Serializers

> Auto-generated documentation for [wa_api.serializers](blob/master/wa_api/serializers.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Wa Api](index.md#wa-api) / Serializers
    - [AdminApiDataSerializer](#adminapidataserializer)
        - [AdminApiDataSerializer().get_policy_type](#adminapidataserializerget_policy_type)
        - [AdminApiDataSerializer().get_roomUrl](#adminapidataserializerget_roomurl)
    - [BanSerializer](#banserializer)
        - [BanSerializer().get_is_banned](#banserializerget_is_banned)
    - [MapSerializer](#mapserializer)
        - [MapSerializer().get_authentication_mandatory](#mapserializerget_authentication_mandatory)
        - [MapSerializer().get_room_slug](#mapserializerget_room_slug)
    - [RelativeUrlRelatedField](#relativeurlrelatedfield)
        - [RelativeUrlRelatedField().to_internal_value](#relativeurlrelatedfieldto_internal_value)
    - [ReportSerializer](#reportserializer)
    - [ReportUserRelatedField](#reportuserrelatedfield)
        - [ReportUserRelatedField().to_internal_value](#reportuserrelatedfieldto_internal_value)
        - [ReportUserRelatedField().to_representation](#reportuserrelatedfieldto_representation)
    - [RoomAccessSerializer](#roomaccessserializer)
    - [RoomSameWorldListSerializer](#roomsameworldlistserializer)
        - [RoomSameWorldListSerializer().to_representation](#roomsameworldlistserializerto_representation)
    - [RoomSameWorldSerializer](#roomsameworldserializer)
    - [TextureSerializer](#textureserializer)
        - [TextureSerializer().get_rights](#textureserializerget_rights)

## AdminApiDataSerializer

[[find in source code]](blob/master/wa_api/serializers.py#L156)

```python
class AdminApiDataSerializer(serializers.ModelSerializer):
```

#### Attributes

- `roomUrl` - wtf is messages?: `serializers.SerializerMethodField()`
- `mapUrlStart` - is this really used somewhere?: `serializers.SlugRelatedField(source='world', sl...`

### AdminApiDataSerializer().get_policy_type

[[find in source code]](blob/master/wa_api/serializers.py#L173)

```python
def get_policy_type(obj):
```

### AdminApiDataSerializer().get_roomUrl

[[find in source code]](blob/master/wa_api/serializers.py#L166)

```python
def get_roomUrl(obj):
```

## BanSerializer

[[find in source code]](blob/master/wa_api/serializers.py#L15)

```python
class BanSerializer(serializers.ModelSerializer):
    def __init__(instance=None, is_banned=False, **kwargs):
```

### BanSerializer().get_is_banned

[[find in source code]](blob/master/wa_api/serializers.py#L23)

```python
def get_is_banned(obj):
```

## MapSerializer

[[find in source code]](blob/master/wa_api/serializers.py#L50)

```python
class MapSerializer(serializers.ModelSerializer):
```

#### Attributes

- `roomSlug` - deprecated, but needs to exist atm 02/22: `serializers.SerializerMethodField(method_name='get_room_slug')`
- `group` - used for some analytics functionality, came across 09/21: `serializers.ReadOnlyField(source='analytics_group')`

### MapSerializer().get_authentication_mandatory

[[find in source code]](blob/master/wa_api/serializers.py#L65)

```python
def get_authentication_mandatory(obj):
```

If the user is anonymous, we turn of oauth even if configured on the world

### MapSerializer().get_room_slug

[[find in source code]](blob/master/wa_api/serializers.py#L62)

```python
def get_room_slug(obj):
```

## RelativeUrlRelatedField

[[find in source code]](blob/master/wa_api/serializers.py#L112)

```python
class RelativeUrlRelatedField(serializers.SlugRelatedField):
```

### RelativeUrlRelatedField().to_internal_value

[[find in source code]](blob/master/wa_api/serializers.py#L113)

```python
def to_internal_value(data):
```

## ReportSerializer

[[find in source code]](blob/master/wa_api/serializers.py#L119)

```python
class ReportSerializer(serializers.ModelSerializer):
```

## ReportUserRelatedField

[[find in source code]](blob/master/wa_api/serializers.py#L88)

```python
class ReportUserRelatedField(RelatedField):
```

### ReportUserRelatedField().to_internal_value

[[find in source code]](blob/master/wa_api/serializers.py#L98)

```python
def to_internal_value(data):
```

### ReportUserRelatedField().to_representation

[[find in source code]](blob/master/wa_api/serializers.py#L89)

```python
def to_representation(value):
```

## RoomAccessSerializer

[[find in source code]](blob/master/wa_api/serializers.py#L130)

```python
class RoomAccessSerializer(serializers.ModelSerializer):
```

#### Attributes

- `anonymous` - messages = SerializerMethodField(method_name="get_messages"): `serializers.BooleanField()`

## RoomSameWorldListSerializer

[[find in source code]](blob/master/wa_api/serializers.py#L185)

```python
class RoomSameWorldListSerializer(serializers.ListSerializer):
```

### RoomSameWorldListSerializer().to_representation

[[find in source code]](blob/master/wa_api/serializers.py#L188)

```python
def to_representation(data):
```

## RoomSameWorldSerializer

[[find in source code]](blob/master/wa_api/serializers.py#L195)

```python
class RoomSameWorldSerializer(MapSerializer):
```

#### See also

- [MapSerializer](#mapserializer)

## TextureSerializer

[[find in source code]](blob/master/wa_api/serializers.py#L31)

```python
class TextureSerializer(serializers.ModelSerializer):
```

#### Attributes

- `rights` - rights = serializers.StringRelatedField(many=True): `serializers.SerializerMethodField(method_name='get_rights')`

### TextureSerializer().get_rights

[[find in source code]](blob/master/wa_api/serializers.py#L38)

```python
def get_rights(obj):
```

This logic is random. We need to end up with a string.
Attribute not used in WA
