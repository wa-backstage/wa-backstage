# Mixins

> Auto-generated documentation for [unicorn_viewsets.mixins](blob/master/unicorn_viewsets/mixins.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Unicorn Viewsets](index.md#unicorn-viewsets) / Mixins
    - [CreateModeMixin](#createmodemixin)
        - [CreateModeMixin().show_create](#createmodemixinshow_create)
        - [CreateModeMixin().validate_and_create](#createmodemixinvalidate_and_create)
    - [DeleteModeMixin](#deletemodemixin)
        - [DeleteModeMixin().delete](#deletemodemixindelete)
    - [UpdateModeMixin](#updatemodemixin)
        - [UpdateModeMixin().show_update](#updatemodemixinshow_update)
        - [UpdateModeMixin().validate_and_update](#updatemodemixinvalidate_and_update)

## CreateModeMixin

[[find in source code]](blob/master/unicorn_viewsets/mixins.py#L1)

```python
class CreateModeMixin():
```

### CreateModeMixin().show_create

[[find in source code]](blob/master/unicorn_viewsets/mixins.py#L2)

```python
def show_create():
```

### CreateModeMixin().validate_and_create

[[find in source code]](blob/master/unicorn_viewsets/mixins.py#L5)

```python
def validate_and_create():
```

## DeleteModeMixin

[[find in source code]](blob/master/unicorn_viewsets/mixins.py#L9)

```python
class DeleteModeMixin():
```

### DeleteModeMixin().delete

[[find in source code]](blob/master/unicorn_viewsets/mixins.py#L10)

```python
def delete(pk):
```

## UpdateModeMixin

[[find in source code]](blob/master/unicorn_viewsets/mixins.py#L15)

```python
class UpdateModeMixin():
```

### UpdateModeMixin().show_update

[[find in source code]](blob/master/unicorn_viewsets/mixins.py#L16)

```python
def show_update(pk):
```

### UpdateModeMixin().validate_and_update

[[find in source code]](blob/master/unicorn_viewsets/mixins.py#L19)

```python
def validate_and_update():
```
