# Views

> Auto-generated documentation for [unicorn_viewsets.views](blob/master/unicorn_viewsets/views.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / [Unicorn Viewsets](index.md#unicorn-viewsets) / Views
    - [ComponentView](#componentview)
        - [ComponentView().get_context_data](#componentviewget_context_data)
    - [GenericModeView](#genericmodeview)
        - [GenericModeView().calling](#genericmodeviewcalling)
        - [GenericModeView().get_errors](#genericmodeviewget_errors)
        - [GenericModeView().get_form](#genericmodeviewget_form)
        - [GenericModeView().get_form_class](#genericmodeviewget_form_class)
        - [GenericModeView().get_form_data](#genericmodeviewget_form_data)
        - [GenericModeView().get_form_kwargs](#genericmodeviewget_form_kwargs)
        - [GenericModeView().get_mode](#genericmodeviewget_mode)
        - [GenericModeView().get_mode_kwargs](#genericmodeviewget_mode_kwargs)
        - [GenericModeView().get_object](#genericmodeviewget_object)
        - [GenericModeView().get_queryset](#genericmodeviewget_queryset)
        - [GenericModeView().get_success_url](#genericmodeviewget_success_url)
        - [GenericModeView().model](#genericmodeviewmodel)
        - [GenericModeView().model](#genericmodeviewmodel)
        - [GenericModeView().needs_page_reload](#genericmodeviewneeds_page_reload)
        - [GenericModeView().prepopulate_fields](#genericmodeviewprepopulate_fields)
        - [GenericModeView().redirect_to_success_url](#genericmodeviewredirect_to_success_url)
        - [GenericModeView().set_attributes](#genericmodeviewset_attributes)
        - [GenericModeView().switch_mode](#genericmodeviewswitch_mode)
        - [GenericModeView().validate_and_save_form](#genericmodeviewvalidate_and_save_form)

## ComponentView

[[find in source code]](blob/master/unicorn_viewsets/views.py#L8)

```python
class ComponentView(TemplateView):
```

### ComponentView().get_context_data

[[find in source code]](blob/master/unicorn_viewsets/views.py#L11)

```python
def get_context_data(**kwargs):
```

## GenericModeView

[[find in source code]](blob/master/unicorn_viewsets/views.py#L15)

```python
class GenericModeView(UnicornView):
    def __init__(**kwargs):
```

### GenericModeView().calling

[[find in source code]](blob/master/unicorn_viewsets/views.py#L139)

```python
def calling(name, args):
```

### GenericModeView().get_errors

[[find in source code]](blob/master/unicorn_viewsets/views.py#L48)

```python
def get_errors(form):
```

### GenericModeView().get_form

[[find in source code]](blob/master/unicorn_viewsets/views.py#L51)

```python
def get_form():
```

### GenericModeView().get_form_class

[[find in source code]](blob/master/unicorn_viewsets/views.py#L54)

```python
def get_form_class():
```

### GenericModeView().get_form_data

[[find in source code]](blob/master/unicorn_viewsets/views.py#L57)

```python
def get_form_data():
```

### GenericModeView().get_form_kwargs

[[find in source code]](blob/master/unicorn_viewsets/views.py#L60)

```python
def get_form_kwargs():
```

### GenericModeView().get_mode

[[find in source code]](blob/master/unicorn_viewsets/views.py#L66)

```python
def get_mode():
```

### GenericModeView().get_mode_kwargs

[[find in source code]](blob/master/unicorn_viewsets/views.py#L73)

```python
def get_mode_kwargs():
```

### GenericModeView().get_object

[[find in source code]](blob/master/unicorn_viewsets/views.py#L76)

```python
def get_object():
```

### GenericModeView().get_queryset

[[find in source code]](blob/master/unicorn_viewsets/views.py#L82)

```python
def get_queryset():
```

### GenericModeView().get_success_url

[[find in source code]](blob/master/unicorn_viewsets/views.py#L88)

```python
def get_success_url():
```

### GenericModeView().model

[[find in source code]](blob/master/unicorn_viewsets/views.py#L37)

```python
@property
def model():
```

### GenericModeView().model

[[find in source code]](blob/master/unicorn_viewsets/views.py#L44)

```python
@model.setter
def model(value):
```

### GenericModeView().needs_page_reload

[[find in source code]](blob/master/unicorn_viewsets/views.py#L91)

```python
def needs_page_reload():
```

### GenericModeView().prepopulate_fields

[[find in source code]](blob/master/unicorn_viewsets/views.py#L119)

```python
def prepopulate_fields(form):
```

override to pass values into form fields automatically
Edit form.data['my_auto_field']

### GenericModeView().redirect_to_success_url

[[find in source code]](blob/master/unicorn_viewsets/views.py#L136)

```python
def redirect_to_success_url():
```

### GenericModeView().set_attributes

[[find in source code]](blob/master/unicorn_viewsets/views.py#L96)

```python
def set_attributes():
```

### GenericModeView().switch_mode

[[find in source code]](blob/master/unicorn_viewsets/views.py#L104)

```python
def switch_mode(mode=None, **kwargs):
```

### GenericModeView().validate_and_save_form

[[find in source code]](blob/master/unicorn_viewsets/views.py#L125)

```python
def validate_and_save_form():
```
