# 0005 Alter Room World

> Auto-generated documentation for [core.migrations.0005_alter_room_world](blob/master/core/migrations/0005_alter_room_world.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0005 Alter Room World
    - [Migration](#migration)

## Migration

[[find in source code]](blob/master/core/migrations/0005_alter_room_world.py#L7)

```python
class Migration(migrations.Migration):
```
