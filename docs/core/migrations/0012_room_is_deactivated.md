# 0012 Room Is Deactivated

> Auto-generated documentation for [core.migrations.0012_room_is_deactivated](blob/master/core/migrations/0012_room_is_deactivated.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0012 Room Is Deactivated
    - [Migration](#migration)

## Migration

[[find in source code]](blob/master/core/migrations/0012_room_is_deactivated.py#L6)

```python
class Migration(migrations.Migration):
```
