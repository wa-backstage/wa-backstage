# 0007 World Slug

> Auto-generated documentation for [core.migrations.0007_world_slug](blob/master/core/migrations/0007_world_slug.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0007 World Slug
    - [Migration](#migration)
    - [do_nothing](#do_nothing)
    - [set_slug](#set_slug)

## Migration

[[find in source code]](blob/master/core/migrations/0007_world_slug.py#L19)

```python
class Migration(migrations.Migration):
```

## do_nothing

[[find in source code]](blob/master/core/migrations/0007_world_slug.py#L15)

```python
def do_nothing(apps, schema_editor):
```

## set_slug

[[find in source code]](blob/master/core/migrations/0007_world_slug.py#L7)

```python
def set_slug(apps, schema_editor):
```
