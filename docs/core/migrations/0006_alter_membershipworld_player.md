# 0006 Alter Membershipworld Player

> Auto-generated documentation for [core.migrations.0006_alter_membershipworld_player](blob/master/core/migrations/0006_alter_membershipworld_player.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0006 Alter Membershipworld Player
    - [Migration](#migration)

## Migration

[[find in source code]](blob/master/core/migrations/0006_alter_membershipworld_player.py#L7)

```python
class Migration(migrations.Migration):
```
