# 0008 Membershipworld Anonymous

> Auto-generated documentation for [core.migrations.0008_membershipworld_anonymous](blob/master/core/migrations/0008_membershipworld_anonymous.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / [Migrations](index.md#migrations) / 0008 Membershipworld Anonymous
    - [Migration](#migration)

## Migration

[[find in source code]](blob/master/core/migrations/0008_membershipworld_anonymous.py#L6)

```python
class Migration(migrations.Migration):
```
