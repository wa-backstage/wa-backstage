# Core

> Auto-generated documentation for [core](blob/master/core/__init__.py) module.

- [Backstage](../README.md#wa-backstage) / [Modules](../MODULES.md#backstage-modules) / Core
    - Modules
        - [Admin](admin.md#admin)
        - [Apps](apps.md#apps)
        - [Management](management/index.md#management)
        - [Migrations](migrations/index.md#migrations)
        - [Models](models.md#models)
        - [Signals](signals.md#signals)
        - [Tests](tests.md#tests)
        - [Utils](utils.md#utils)
