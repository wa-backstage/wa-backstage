# Management

> Auto-generated documentation for [core.management](blob/master/core/management/__init__.py) module.

- [Backstage](../../README.md#wa-backstage) / [Modules](../../MODULES.md#backstage-modules) / [Core](../index.md#core) / Management
    - Modules
        - [Commands](commands/index.md#commands)
