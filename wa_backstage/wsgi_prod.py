"""
WSGI config for wa_backstage project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/

This config includes whitenoise module to server static files in production builds.
"""

import os

from django.core.wsgi import get_wsgi_application

from whitenoise import WhiteNoise

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "wa_backstage.settings.production")

application = get_wsgi_application()
application = WhiteNoise(application, root="/app/static")
application.add_files("/app/media", prefix="media/")
