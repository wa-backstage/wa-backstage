from .common import *  # noqa: F401, F403

DEBUG = False

# add whitenoise middleware for static file serving

for index, middleware in enumerate(MIDDLEWARE):  # noqa f405
    # insert whitenoise as documented behind the following middleware
    if middleware == "django.middleware.security.SecurityMiddleware":
        MIDDLEWARE.insert(index, "whitenoise.middleware.WhiteNoiseMiddleware")  # noqa f405

# these settings should be set in local.py

SECRET_KEY = ""

ALLOWED_HOSTS = []

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "fill it",
        "USER": "fill it",
        "PASSWORD": "fill it",
        "HOST": "fill it",
        "PORT": "",
    }
}
EMAIL_HOST = "fill it"
EMAIL_HOST_USER = "fill it"
EMAIL_HOST_PASSWORD = "fill it"
EMAIL_PORT = 25
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = "fill it"

SITE_URL = "fill it"

# load local.py settings if they exist

try:
    from .local import *
except ImportError:
    pass
