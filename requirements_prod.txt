-r requirements.txt
# application server
gunicorn==20.0.*
# static file serving
whitenoise==5.3.*
# use postgres in production
psycopg2==2.9.*
