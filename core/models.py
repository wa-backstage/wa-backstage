# python imports
import uuid

# django imports
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models

from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator

# from django.contrib.admin import templates.registration.pa
# projects import
from core.utils import SingletonModel


class PolicyType(models.IntegerChoices):
    ANONYMOUS = 1, _("Any player may use")
    MEMBERS_ONLY = 2, _("Only world members may use")
    USE_TAGS = 3, _("Players with room tags may use")


class BackstageUser(AbstractUser):
    email = models.EmailField(_("email"), unique=True)


class Tag(models.Model):
    name = models.CharField(max_length=80, verbose_name=_("Name"))
    description = models.TextField(verbose_name=_("Description"), blank=True)

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")
        ordering = ["name"]

    def __str__(self):
        return self.name


class AdminTag(SingletonModel, Tag):
    class Meta:
        verbose_name = _("Admin Tag")

    def __eq__(self, other):
        assert isinstance(other, Tag)
        pk = self.load().pk
        if pk is None:
            return self is other
        return pk == other.pk

    def save(self, *args, **kwargs):
        self.name = "admin"
        return super().save(*args, **kwargs)


class CharacterTexture(models.Model):
    name = models.CharField(max_length=80, verbose_name=_("Name"))
    texture_id = models.IntegerField(verbose_name=_("Texture ID"))
    level = models.IntegerField(verbose_name=_("Level"))
    # rights implemented in WA without any functionality 02/22
    # maybe some tagging feature was planed once
    # not implemented in WA Code
    # a wa-backstage implementation is provided
    policy_type = models.IntegerField(
        verbose_name=_("Policy Type"), choices=PolicyType.choices, default=PolicyType.ANONYMOUS
    )
    tags = models.ManyToManyField(Tag, verbose_name=_("Tags"), blank=True)
    worlds = models.ManyToManyField(
        "World", through="core.world_character_textures", verbose_name=_("Worlds"), blank=True
    )
    url = models.CharField(max_length=1024, verbose_name=_("URL"))
    is_deactivated = models.BooleanField(verbose_name=_("Is deactivated?"), default=False)

    class Meta:
        verbose_name = _("Character Texture")
        verbose_name_plural = _("Character Textures")
        ordering = ["texture_id"]


class World(models.Model):
    name = models.CharField(max_length=80, verbose_name=_("Name"))
    slug = models.CharField(max_length=100, blank=True, unique=True)
    description = models.TextField(verbose_name=_("Description"), blank=True)
    url = models.CharField(max_length=1024, verbose_name=_("URL"), blank=True)
    players = models.ManyToManyField("Player", through="MembershipWorld", related_name="worlds")
    contact_page = models.CharField(max_length=1024, verbose_name=_("Contact Page"), blank=True)
    # oauth provider is configured per WA instance env
    # this is automatically turned off for policy_type 1
    use_oauth = models.BooleanField(verbose_name=_("Redirect to oauth provider?"), default=False)
    character_textures = models.ManyToManyField(
        CharacterTexture,
        verbose_name=_("Character Texture"),
        blank=True,
    )

    class Meta:
        verbose_name = _("World")
        verbose_name_plural = _("Worlds")
        ordering = ["name"]

    def __str__(self):
        return self.name


class Room(models.Model):
    name = models.CharField(max_length=80, verbose_name=_("Name"))
    description = models.TextField(verbose_name=_("Description"), blank=True)
    url = models.CharField(max_length=1024, verbose_name=_("URL"), blank=True)
    map_url = models.URLField(
        max_length=1024,
        verbose_name=_("Map URL"),
        blank=True,
        error_messages={"invalid": "You need to enter a valid URL that also points to a JSON file."},
        validators=[RegexValidator(r"[\s\S]*\.json$", message=_("Map URL needs to point to a json file."))],
    )
    is_entry_room = models.BooleanField(verbose_name=_("Is entry room of world?"))
    world = models.ForeignKey(World, verbose_name=_("World"), on_delete=models.CASCADE, related_name="rooms")
    room_contact_page = models.CharField(max_length=1024, verbose_name=_("Contact Page"), blank=True)
    analytics_group = models.CharField(max_length=80, verbose_name=_("Name"), blank=True)

    tags = models.ManyToManyField(
        Tag,
        related_name="room_tags",
        verbose_name=_("Room Tags"),
        blank=True,
    )
    policy_type = models.IntegerField(
        verbose_name=_("Policy Type"), choices=PolicyType.choices, default=PolicyType.MEMBERS_ONLY
    )
    is_deactivated = models.BooleanField(verbose_name=_("Is deactivated?"), default=False)

    @property
    def contact_page(self):
        """
        We use contact page URL of world if not specified on room
        """
        if self.room_contact_page:
            return self.room_contact_page
        else:
            return self.world.contact_page

    class Meta:
        verbose_name = _("Room")
        verbose_name_plural = _("Rooms")
        ordering = ["name"]

    def __str__(self):
        return self.name


class Player(models.Model):
    username = models.CharField(max_length=80, verbose_name=_("Username"))
    email = models.EmailField(verbose_name=_("Email Address"))
    comment = models.TextField(verbose_name=_("Comment"), blank=True)

    class Meta:
        verbose_name = _("Player")
        verbose_name_plural = _("Players")
        ordering = ["username"]

    def __str__(self):
        return self.username


class MembershipWorldQuerySet(models.QuerySet):
    def exclude_anonymous(self):
        return self.exclude(anonymous=True)


class MembershipWorld(models.Model):
    uuid_token = models.UUIDField(verbose_name=_("Access Token"), primary_key=True, default=uuid.uuid4)
    player = models.ForeignKey(
        Player, verbose_name=_("Player"), on_delete=models.CASCADE, related_name="world_memberships"
    )
    world = models.ForeignKey(World, verbose_name=_("World"), on_delete=models.CASCADE, related_name="memberships")
    tags = models.ManyToManyField(Tag, verbose_name=_("Tags"), blank=True)
    character_textures = models.ManyToManyField(CharacterTexture, verbose_name=_("Character Textures"), blank=True)
    anonymous = models.BooleanField(default=False, verbose_name=_("Anonymous Player?"))
    visit_card_url = models.CharField(max_length=1024, verbose_name=_("Visit card url"), blank=True)

    objects = models.Manager.from_queryset(MembershipWorldQuerySet)()

    class Meta:
        verbose_name = _("World Membership")
        verbose_name_plural = _("World Memberships")
        ordering = ["player__username"]
        unique_together = ["player", "world"]

    def __str__(self):
        return _("Membership: ") + self.player.username

    def get_world_access_url(self):
        return settings.WORLD_ACCESS_URL_FORMAT.format(world_url=self.world.url, access_token=self.uuid_token)

    def is_admin(self):
        return AdminTag() in self.tags.all()


class Ban(models.Model):
    membership_world = models.ForeignKey(
        MembershipWorld, verbose_name=_("World Membership"), on_delete=models.CASCADE, related_name="bans"
    )
    room = models.ForeignKey(
        Room,
        verbose_name=_("Room"),
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="bans",
    )
    message = models.CharField(max_length=140, verbose_name=_("Message"), blank=True)

    class Meta:
        verbose_name = _("Ban")
        verbose_name_plural = _("Bans")
        # unique_together = [["membership_world", "room"]]
        constraints = [
            models.UniqueConstraint(fields=["membership_world", "room"], name="unique_room_ban"),
            models.UniqueConstraint(
                fields=["membership_world"], condition=models.Q(room=None), name="unique_world_ban"
            ),
        ]

    def __str__(self):
        player_name = self.membership_world.player.username
        # world_name = self.membership_world.world.name
        # room_name = f"({self.room.name})" if self.room else ""
        return _("{}").format(player_name)


class Report(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Report time"))
    room = models.ForeignKey(Room, verbose_name=_("Room"), on_delete=models.CASCADE, related_name="reports")
    user_comment = models.CharField(max_length=250, verbose_name=_("Comment"))
    reported_user = models.ForeignKey(
        Player, verbose_name=_("Player"), on_delete=models.CASCADE, related_name="reported"
    )
    reporter_user = models.ForeignKey(
        Player, verbose_name=_("Player"), on_delete=models.CASCADE, related_name="reporter"
    )

    class Meta:
        verbose_name = _("Report")
        verbose_name_plural = _("Reports")
        ordering = ["-created_at"]
